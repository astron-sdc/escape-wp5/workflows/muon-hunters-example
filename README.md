## Zooniverse - Muon Hunters Example of ESAP Shopping Basket

The ESAP Archives (accessible via the ESAP GUI) include data retrieval from the Zooniverse Classification Database using the ESAP Shopping Basket. This directory contains resources for the _Muon Hunters Example_ tutorial, which focuses on loading Zooniverse data from a saved shopping basket into a notebook and performing simple aggregation of the classification results. This directory is also available as an Interactive Analysis workflow in the ESAP GUI.

This tutorial forms part of a series of advanced guides for managing Zooniverse projects through Python. While they can be done independently, for best usage you may want to complete them in the following order (these are also all available as Interactive Analysis workflows in the ESAP GUI):
1. [Advanced Project Building](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-project-building)
2. [Muon Hunters Example (current)](https://git.astron.nl/astron-sdc/escape-wp5/workflows/muon-hunters-example)
3. [Advanced Aggregation with Caesar](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-aggregation-with-caesar)
4. [Integrating Machine Learning](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-integrating-machine-learning)

For guides on the basics of creating a Zooniverse project and workflow through the Zooniverse web interface, see the [Zooniverse 'Getting Started' guide](https://help.zooniverse.org/getting-started/) and ['Best Practices' guide](https://help.zooniverse.org/best-practices/). A [recorded demonstration](https://youtu.be/zJJjz5OEUAw?t=7633) and [accompanying slides](https://indico.in2p3.fr/event/21939/contributions/89045/) of using this interface are available as part of the [First ESCAPE Citizen Science Workshop](https://indico.in2p3.fr/event/21939/).

The advanced tutorial presented here includes demonstrations of using Python for:
* Importing the Panoptes Python Client & ESAP client Zooniverse connector
* Retrieving items added to an ESAP Shopping Basket
* Example retrieval & analysis of data from the Muon Hunter Zooniverse project
    * Downloading & processing classification data
    * Plotting the data and performing simple aggregation of the results

You can find the code for the tutorial in the `notebooks` folder.
This tutorial makes use of example material (subjects, metadata, classifications) from the [_Muon Hunter_](https://www.zooniverse.org/projects/zooniverse/muon-hunter-classic) Zooniverse project, which involves classifying images from VERITAS telescopes in search of muon detections, which are characterised by ring-like structures. However, knowledge of the details of the project is not necessary for this example tutorial.

### Setup - ESAP workflow as a remote notebook instance

You may need to install the `panoptes_client` and `pandas` packages.
```
!python -m pip install panoptes_client
!python -m pip install pandas
```
As this tutorial uses the ESAP Shopping Basket, it can only be run as an ESAP Interactive Analysis workflow (i.e. inside the Data Lake as a Service (DLaaS) environment).

### Other Useful Resources

Here is a list of additional resources that you may find useful when building your own Zooniverse citizen science project.
* [_Zooniverse_ website](http://zooniverse.org) - Interested in Citizen Science? Create a **free** _Zooniverse_ account, browse other projects for inspiration, contribute yourself as a citizen scientist, and build your own project.
* [Zooniverse project builder help pages](https://help.zooniverse.org) - A great resource with practical guidance, tips and advice for building great Citizen Science projects. See the ["Building a project using the project builder"](https://youtu.be/zJJjz5OEUAw?t=7633) recorded tutorial for more information.
* [_Caesar_ web interface](https://caesar.zooniverse.org) - An online interface for the _Caesar_ advanced retirement and aggregation engine. See the ["Introducing Caesar"](https://youtu.be/zJJjz5OEUAw?t=10830) recorded tutorial for tips and advice on how to use _Caesar_ to supercharge your _Zooniverse_ project.
* [The `panoptes_client` documentation](https://panoptes-python-client.readthedocs.io/en/v1.1/) - A comprehensive reference for the Panoptes Python Client.
* [The `panoptes_aggregation` documentation](https://aggregation-caesar.zooniverse.org/docs) - A comprehensive reference for the Panoptes Aggregation tool.

This tutorial is adapted from https://github.com/stvoutsin/esap_demo.