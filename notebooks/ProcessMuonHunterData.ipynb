{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Zooniverse - Muon Hunter Example of ESAP Shopping Basket\n",
    "\n",
    "The ESAP Archives (accessible via the ESAP GUI) include data retrieval from the Zooniverse Classification Database using the ESAP Shopping Basket. In this tutorial, we will examine how to load Zooniverse data from a saved ESAP Shopping Basket into a notebook and performing simple aggregation of the classification results.\n",
    "\n",
    "As an example, this tutorial uses classification data from the [_Muon Hunter_](https://www.zooniverse.org/projects/zooniverse/muon-hunter-classic) Zooniverse project, which involves classifying images from VERITAS telescopes in search of muon detections, which are characterised by ring-like structures. However, access to this data will not be available to you, so you should adapt the notebook to your own project to be able to run the code presented here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": "true"
   },
   "source": [
    "## Table of Contents\n",
    "\n",
    "1. [Setup](#Setup)\n",
    "2. [Add Items to Shopping Basket](#Add-Items-to-Shopping-Basket)\n",
    "3. [Retrieve Shopping Basket Data](#Retrieve-Shopping-Basket-Data)\n",
    "4. [Example: Analysing Data from the Muon Hunter Zooniverse Project](#Example:-Analysing-Data-from-the-Muon-Hunter-Zooniverse-Project)\n",
    "5. [Load Classification Data](#Load-Classification-Data)\n",
    "6. [Process Classification Data](#Process-Classification-Data)\n",
    "7. [Plot Some of the Muon Ring Properties](#Plot-Some-of-the-Muon-Ring-Properties)\n",
    "8. [Do Some Simple Aggregation](#Do-Some-Simple-Aggregation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import io\n",
    "import json\n",
    "import getpass\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as mplplot\n",
    "%matplotlib inline\n",
    "from IPython import display"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may need to install the `panoptes_client` and `pandas` packages. If you do, then run the code in the next cell. Otherwise, skip it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!python -m pip install panoptes_client\n",
    "!python -m pip install pandas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As this tutorial uses the ESAP Shopping Basket, it can only be run as an ESAP Interactive Analysis workflow through the Science Analysis Platform (i.e. inside the Data Lake as a Service (DLaaS) environment). If for whatever reason you need to install the `esap_client` package, this can be done by running the code in the next cell or otherwise by following the installation procedure given here: https://git.astron.nl/astron-sdc/escape-wp5/esap-userprofile-python-client"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!python -m pip install git+https://git.astron.nl/astron-sdc/esap-userprofile-python-client.git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Import the Panoptes Python Client\n",
    "\n",
    "Panoptes is the name of the backend API that drives the Zooniverse citizen science platform. It provides a RESTful API with to the database of Zooniverse projects and workflows, as well as the classifications provided by citizen scientists.\n",
    "\n",
    "The Panoptes Python Client allows you to manage your project using Python scripts, including through requesting data exports for your projects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from panoptes_client import Subject"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import the ESAP client Zooniverse connector\n",
    "\n",
    "This connector allows the ESAP client to access data exports from the Zooniverse classification database. Data retrieval is facilitated by a RESTful web API. The ESAP platform makes use of a Python client library provided by the Zooniverse development team."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from esap_client import ShoppingClient as shopping_client\n",
    "from esap_client.connectors import Zooniverse as zooniverse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Add Items to Shopping Basket\n",
    "\n",
    "If you have not done so already, use the [ESAP interface](https://sdc-dev.astron.nl/esap-gui/) to add items to your ESAP Shopping Basket. To do so:\n",
    "1. Click on \"Visit Zooniverse Archives\".\n",
    "2. On the right, click \"Query this Dataset\".\n",
    "3. Select the catalogue you want (Project or Workflow) and enter your Panoptes account username and password (these are the same as your Zooniverse account). Another option, \"Extra Project Fields\", will appear, but for now we'll leave that as the default (all fields).\n",
    "4. Click \"Submit\".\n",
    "5. Scroll down and select the project or workflow data you would like, ticking \"Select Classification Data\" for the volunteer classifications and/or \"Select Subject Data\" for various metadata on each subject.\n",
    "6. Finally, click \"Save Basket\" at the top right of the interface."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Retrieve Shopping Basket Data\n",
    "\n",
    "Next, we shall use the ESAP client Zooniverse connector to retrieve the contents of the Shopping Basket relating to Zooniverse.\n",
    "\n",
    "This requires your Zooniverse account username/email and password."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "zooniverseUsername = input(\"Enter Zooniverse account username: \")\n",
    "zooniversePassword = getpass.getpass(\"Enter Zooniverse account password: \")\n",
    "zooniverseConnector = zooniverse(username=zooniverseUsername, password=zooniversePassword)\n",
    "shoppingClient = shopping_client(host=\"https://sdc-dev.astron.nl:443/\", connectors=[zooniverseConnector])\n",
    "basket = shoppingClient.get_basket(convert_to_pandas=True)\n",
    "basket"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Render basket `DataFrame` nicely."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "basket[\"zooniverse\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Select an item of interest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item = basket[\"zooniverse\"].iloc[0]\n",
    "item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: Analysing Data from the Muon Hunter Zooniverse Project\n",
    "\n",
    "We can also create a dummy basket here to retrieve data from Zooniverse, rather than using the shopping basket in the ESAP GUI.\n",
    "\n",
    "Just as for the ESAP GUI, we'll specify the `archive` as `zooniverse`, set either `workflow` or `project` as the `catalog`, enter the `project_id` and `workflow_id`, and specify `classifications` or `subjects` as the `category` of data we want.\n",
    "\n",
    "As an example, the below creates a dummy basket `item` for Muon Hunter classifications (replacing the `item` above), but you can replace the `project_id` and `workflow_id` with your own if you have them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item = pd.Series(json.loads({\"item_data\":'{\"archive\":\"zooniverse\",\"catalog\":\"workflow\",\"project_id\":\"3098\",\"workflow_id\":\"2473\",\"category\":\"classifications\"}'}[\"item_data\"]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also check to make sure we can actually connect to this `item` through the `zooniverseConnector`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if zooniverseConnector.is_available(item):\n",
    "    print(\"Item is available\")\n",
    "else:\n",
    "    print(\"Retrieval Failed\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Classification Data\n",
    "\n",
    "Now, let's retrieve the requested `item` from Zooniverse.\n",
    "\n",
    "As with requesting a Data Export from Zooniverse, `zooniverseConnector.retrieve` contains the `generate` and `wait` keywords:\n",
    "- `generate` - If true then the requested data export will be generated if it does not already exist and regenerated if it does. The default is False.\n",
    "- `wait` - If (re)generation is already in progress and you run `zooniverseConnect.retrieve` with `generate=False` and `wait=True`, then your program will wait for the new export to be generated and then download it (remember, for large exports this might take a long time). If instead `wait=False` (the default), the program will not wait and instead download the current data export (if available). Has no effect if `generate` is `True`.\n",
    "\n",
    "#### Optional: Workaround for low-memory environments\n",
    "The full classification set for Muon Hunter contains millions of rows. For this example, to limit the memory footprint of retrieving the data, we'll retrieve it in chunks of 1000 rows and only retrieve $10^{5}$ rows in total."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classificationDataFrame = zooniverseConnector.retrieve(item, \n",
    "                                                       generate=False, \n",
    "                                                       wait=True, \n",
    "                                                       convert_to_pandas=True,\n",
    "                                                       chunked_retrieve=True, \n",
    "                                                       chunk_size=1000, \n",
    "                                                       nrows=int(1e5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Save a local copy of the data. By default this is using the current working directory, but feel free to change this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classificationDataFrame.to_pickle(\"muonHunterJustInCase.pkl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "View the retrieved data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classificationDataFrame"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Process Classification Data\n",
    "\n",
    "Next, let's filter the retrieved classification data down to what we want, using `pandas` for data manipulation, before we extract the data.\n",
    "\n",
    "This data is unique to each project depending on the tasks that volunteers are asked to do. So, if you wish to modify this example to work with your own data, you may want to first examine how your data is stored by downloading it from the Data Exports tab in Zooniverse's Project Builder web interface. These exports are `.csv` files, so can be viewed in a Jupyter Notebook using `pandas.read_csv(<file>)` or opened in Microsoft Excel, for example.\n",
    "\n",
    "### Isolate classifications since the project went live\n",
    "\n",
    "In this example, we'll limit our data to those classified after a given date (the date the project went live, so avoiding any classifications from the beta phase). We'll also specify the workflow ID and the column headers we want."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "annotationsAndSubjects = classificationDataFrame[\n",
    "    (classificationDataFrame.created_at > \"2017-01-23T00:00:00.00Z\") & \n",
    "    (classificationDataFrame.workflow_id == 2473)][['annotations', 'subject_ids', 'subject_data']]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize arrays to store parsed data\n",
    "\n",
    "Prior to extraction, let's initialise the arrays we want to store the data in.\n",
    "\n",
    "For Muon Hunter, the extracted data are:\n",
    "1. The subject IDs - These Zooniverse-assigned values can be used to reference the original subjects.\n",
    "2. The ring indices - Some classifiers may have drawn multiple rings for the same subject - in this case the ring index enumerates those multiple rings (`int` or `NaN`)\n",
    "3. The geometrical properties of each ring:\n",
    "    1. The ring centre coordinates (`float` or `NaN`)\n",
    "    2. The ring radius (`float` or `NaN`)\n",
    "    3. The ring angle - this is superflous, since the ring is circular(`float` or `NaN`).\n",
    "4. The camera containment of each ring:\n",
    "    1. Whether the ring is fully contained within the camera (`Boolean`).\n",
    "    2. Whether the portion of the ring in the camera is complete (`Boolean`).\n",
    "5. The initial classification of the subject - derived from the filename:\n",
    "    1. Whether the initial classification was *Muon* (`Boolean`).\n",
    "    2. Whether the initial classification was *Non-Muon* (`Boolean`).\n",
    "    3. Whether the initial classification was *Ambiguous* (`Boolean`).\n",
    "6. The filename (`str`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "parsedAnnotations = []\n",
    "subjectIds = []\n",
    "ringIndices = []\n",
    "ringPresent = []\n",
    "ringCentre = []\n",
    "ringRadius = []\n",
    "ringAngle = []\n",
    "containedRing = []\n",
    "completeRing = []\n",
    "initialMuon = []\n",
    "initialNonMuon = []\n",
    "initialAmbiguous = []\n",
    "fileName = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extract annotation data\n",
    "\n",
    "Now to get extracting. Note that some care must be taken to ensure that expected data are present and supply a suitable null value otherwise: Zooniverse data dumps are not very intelligent!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "for annotationsAndSubject in annotationsAndSubjects.iterrows() :\n",
    "    annotation = annotationsAndSubject[1].annotations\n",
    "    subjectData = json.loads(annotationsAndSubject[1].subject_data)\n",
    "    try:\n",
    "        subjectDatum = next(iter(subjectData.values()))\n",
    "        currentFileName = subjectDatum[list(subjectDatum.keys())[-1]]\n",
    "        if not currentFileName.endswith(\".jpeg\"):\n",
    "            print(currentFileName)\n",
    "    except KeyError as e:\n",
    "        pass\n",
    "\n",
    "    for task in annotation :\n",
    "        if 'T1' in task['task'] :\n",
    "            if 'value' in task and task['value'] is not None and 'Yes' not in task['value']:\n",
    "                subjectIds.append(int(annotationsAndSubject[1].subject_ids))\n",
    "                ringIndices.append(None)\n",
    "                ringPresent.append(False)\n",
    "                ringCentre.append((None, None))\n",
    "                ringRadius.append(None)\n",
    "                ringAngle.append(None)\n",
    "                containedRing.append(False)\n",
    "                completeRing.append(False)\n",
    "                parsedAnnotations.append(annotation)\n",
    "                fileName.append(currentFileName)\n",
    "                initialNonMuon.append('non_muon' in currentFileName)\n",
    "                initialMuon.append('muon' in currentFileName and not initialNonMuon[-1])\n",
    "                initialAmbiguous.append(not (initialMuon[-1] or initialNonMuon[-1]))\n",
    "                continue\n",
    "        elif 'T0' in task['task'] : # there must be a ring\n",
    "            for ringIndex, ringData in enumerate(task['value']) :\n",
    "                subjectIds.append(int(annotationsAndSubject[1].subject_ids))\n",
    "                ringIndices.append(ringIndex)\n",
    "                ringPresent.append(True)\n",
    "                ringCentre.append((ringData['x'] if 'x' in ringData else None, ringData['y'] if 'y' in ringData else None))\n",
    "                ringRadius.append(ringData['r'] if 'r' in ringData else None)\n",
    "                ringAngle.append(ringData['angle'] if 'angle' in ringData else None)\n",
    "                containedRing.append(None)\n",
    "                completeRing.append(None)\n",
    "                parsedAnnotations.append(annotation)\n",
    "                fileName.append(currentFileName)\n",
    "                initialNonMuon.append('non_muon' in currentFileName)\n",
    "                initialMuon.append('muon' in currentFileName and not initialNonMuon[-1])\n",
    "                initialAmbiguous.append(not (initialMuon[-1] or initialNonMuon[-1]))\n",
    "                for nestedTask in annotation :\n",
    "                    if 'T3' in nestedTask['task'] :\n",
    "                        containedRing[-1] = ('Yes' in nestedTask['value'])\n",
    "                    if 'T5' in nestedTask['task'] :\n",
    "                        completeRing[-1] = ('Yes' in nestedTask['value'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Store extracted data\n",
    "\n",
    "Use a `pandas.DataFrame` to store the extracted data, since this makes subsequent data manipulation more straightforward."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "classificationsFrame = pd.DataFrame({'subjectId' : subjectIds, \n",
    "                                     'ringIndex' : ringIndices, \n",
    "                                     'ringPresent' : ringPresent, \n",
    "                                     'ringCentreX' : [x for x, _ in ringCentre], \n",
    "                                     'ringCentreY' : [y for _, y in ringCentre],\n",
    "                                     'ringAngle' : ringAngle, \n",
    "                                     'ringRadius' : ringRadius,\n",
    "                                     'containedRing' : containedRing, \n",
    "                                     'completeRing' : completeRing, \n",
    "                                     'initialNonMuon' : initialNonMuon,\n",
    "                                     'initialMuon' : initialMuon,\n",
    "                                     'initialAmbiguous' : initialAmbiguous,\n",
    "                                     'fileName' : fileName,\n",
    "                                     'fullAnnotation': parsedAnnotations}).set_index('subjectId')\n",
    "display(classificationsFrame)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot Some of the Muon Ring Properties\n",
    "\n",
    "Having finished data extraction, let's go ahead and take a look at the classifications by plotting some of the results, using histograms to show the distribution of volunteers' classification responses (as each subject will have been classified by multiple volunteers).\n",
    "\n",
    "The following is very much specific to the Muon Hunter data, but showcases how one might access and analyse the resulting classifications.\n",
    "\n",
    "### First in Scalable Vector Graphics (SVG) space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Focus only on images classified as containing muon rings\n",
    "withRingCut = classificationsFrame['ringPresent']\n",
    "withRingFigure = mplplot.figure(figsize=(12,4))\n",
    "\n",
    "# 2D hexagonal binning plot of muon ring centre coordinates (pixels)\n",
    "ringCentreAxes = withRingFigure.add_subplot(1, 3, 1)\n",
    "ringCentreAxes = classificationsFrame[withRingCut].plot.hexbin(ax=ringCentreAxes, x='ringCentreX', y='ringCentreY', gridsize=20, cmap='viridis', sharex=False, norm=matplotlib.colors.LogNorm())\n",
    "ringCentreAxes.set_ylabel('Y (SVG Pixels)')\n",
    "ringCentreAxes.set_xlabel('X (SVG Pixels)')\n",
    "ringCentreAxes.set_title('Ring centre coordinates\\n(SVG Canvas Space)')\n",
    "\n",
    "# Histogram of muon ring radii (pixels)\n",
    "ringRadiusAxes = withRingFigure.add_subplot(1, 3, 2)\n",
    "ringRadiusAxes = classificationsFrame[withRingCut].ringRadius.plot.hist(ax=ringRadiusAxes, bins=100, histtype='step', fill=True, alpha=0.2, color='r', facecolor='r')\n",
    "ringRadiusAxes.set_ylabel('Entries')\n",
    "ringRadiusAxes.set_xlabel('Radius (SVG Pixels)')\n",
    "ringRadiusAxes.set_title('Ring Radii\\n(SVG Canvas Space)')\n",
    "\n",
    "# Histogram of muon ring angle (degrees)\n",
    "ringAngleAxes = withRingFigure.add_subplot(1, 3, 3)\n",
    "ringAngleAxes = classificationsFrame[withRingCut].ringAngle.plot.hist(ax=ringAngleAxes, bins=100, range=(-180, 180),histtype='step', fill=True, alpha=0.2, color='b', facecolor='b')\n",
    "ringAngleAxes.set_ylabel('Entries')\n",
    "ringAngleAxes.set_xlabel('Angle (Degrees)')\n",
    "ringAngleAxes.set_title('Ring Angles')\n",
    "\n",
    "mplplot.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Now in angular camera coordinates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# Define class to convert SVG space to VERITAS camera resolution\n",
    "class SVGToVERITASCamera :\n",
    "    \n",
    "    defaultCameraCentreSVG = (1050, 1050)\n",
    "    # vertex-to-vertex diameter\n",
    "    defaultPixelV2VDiameterSVG = 75\n",
    "    defaultPixelV2VDiameterDegrees = 0.144\n",
    "    # face-to-face diameter\n",
    "    defaultPixelF2FDiameterSVG = 60\n",
    "    # camera dimensions\n",
    "    defaultCameraWidthSVG = 1620\n",
    "    defaultCameraHeightSVG = 1640\n",
    "    \n",
    "    \n",
    "    def __init__(self) :\n",
    "        self.svgPixelsPerDegree = SVGToVERITASCamera.defaultPixelV2VDiameterSVG/SVGToVERITASCamera.defaultPixelV2VDiameterDegrees\n",
    "    \n",
    "    def mapSVGPixelCoordsToDegrees(self, coordinates) :\n",
    "        return ((coordinates[0] - SVGToVERITASCamera.defaultCameraCentreSVG[0]/2)/self.svgPixelsPerDegree,\n",
    "                (coordinates[1] - SVGToVERITASCamera.defaultCameraCentreSVG[1]/2)/self.svgPixelsPerDegree)\n",
    "    \n",
    "    def mapSVGPixelLengthToDegrees(self, length) :\n",
    "        return length/self.svgPixelsPerDegree\n",
    "    \n",
    "    # TODO: Add more mappings as required"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "coordMapper = SVGToVERITASCamera()\n",
    "\n",
    "# Convert each muon ring's central coordinates and radius from SVG pixels to degrees\n",
    "mappedX, mappedY, mappedR = zip(*tuple([tuple([*coordMapper.mapSVGPixelCoordsToDegrees(coords), coordMapper.mapSVGPixelLengthToDegrees(radius)]) for coords, radius in zip(zip(classificationsFrame.ringCentreX, classificationsFrame.ringCentreY),classificationsFrame.ringRadius)]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Add results to dataframe\n",
    "classificationsFrame['ringCentreXDeg'] = mappedX\n",
    "classificationsFrame['ringCentreYDeg'] = mappedY\n",
    "classificationsFrame['ringRadiusDeg'] = mappedR\n",
    "display(classificationsFrame)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "withRingFigure = mplplot.figure(figsize=(12,4))\n",
    "\n",
    "# 2D hexagonal binning plot of muon ring centre coordinates (degrees)\n",
    "ringCentreAxes = withRingFigure.add_subplot(1, 3, 1)\n",
    "ringCentreAxes = classificationsFrame[withRingCut].plot.hexbin(ax=ringCentreAxes, x='ringCentreXDeg', y='ringCentreYDeg', gridsize=20, cmap='viridis', sharex=False, norm=matplotlib.colors.LogNorm())\n",
    "ringCentreAxes.set_ylabel('Y (Degrees)')\n",
    "ringCentreAxes.set_xlabel('X (Degrees)')\n",
    "ringCentreAxes.set_title('Ring centre coordinates\\n(Angular Space)')\n",
    "\n",
    "# Histogram of muon ring radii (degrees)\n",
    "ringRadiusAxes = withRingFigure.add_subplot(1, 3, 2)\n",
    "ringRadiusAxes = classificationsFrame[withRingCut].ringRadiusDeg.plot.hist(ax=ringRadiusAxes, bins=100, histtype='step', fill=True, alpha=0.2, color='r', facecolor='r')\n",
    "ringRadiusAxes.set_ylabel('Entries')\n",
    "ringRadiusAxes.set_xlabel('Radius (Degrees)')\n",
    "ringRadiusAxes.set_title('Ring Radii\\n(Angular Space)')\n",
    "\n",
    "# Histogram of muon ring angle (degrees) (same as before)\n",
    "ringAngleAxes = withRingFigure.add_subplot(1, 3, 3)\n",
    "ringAngleAxes = classificationsFrame[withRingCut].ringAngle.plot.hist(ax=ringAngleAxes, bins=100, range=(-180, 180),histtype='step', fill=True, alpha=0.2, color='b', facecolor='b')\n",
    "ringAngleAxes.set_ylabel('Entries')\n",
    "ringAngleAxes.set_xlabel('Angle (Degrees)')\n",
    "ringAngleAxes.set_title('Ring Angles')\n",
    "\n",
    "mplplot.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Check whether volunteers agree with automatic classification\n",
    "\n",
    "Muon Hunter also contains data from initial automated classification, so for each category that images were initially automatically classified into (`'initialNonMuon'`, `'initialMuon'`, `'initialAmbiguous'`), let's see the distributions of volunteer responses to get a general idea for how well the automated classification worked."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "agreementFigure = mplplot.figure(figsize=(12,4))\n",
    "\n",
    "nonMuonAxes = agreementFigure.add_subplot(1, 3, 2)\n",
    "nonMuonAxes = classificationsFrame[classificationsFrame['initialNonMuon']].ringPresent.astype(int).plot.hist(ax=nonMuonAxes, fill=True, alpha=0.2, color='r', facecolor='r')\n",
    "nonMuonAxes.set_ylabel('Entries')\n",
    "nonMuonAxes.set_xlabel('Ring Present')\n",
    "nonMuonAxes.set_title('Initial Non-Muons')\n",
    "\n",
    "muonAxes = agreementFigure.add_subplot(1, 3, 1)\n",
    "muonAxes = classificationsFrame[classificationsFrame['initialMuon']].ringPresent.astype(int).plot.hist(ax=muonAxes, fill=True, alpha=0.2, color='g', facecolor='g')\n",
    "muonAxes.set_ylabel('Entries')\n",
    "muonAxes.set_xlabel('Ring Present')\n",
    "muonAxes.set_title('Initial Muons')\n",
    "\n",
    "ambiguousAxes = agreementFigure.add_subplot(1, 3, 3)\n",
    "ambiguousAxes = classificationsFrame[classificationsFrame['initialAmbiguous']].ringPresent.astype(int).plot.hist(ax=ambiguousAxes, fill=True, alpha=0.2, color='b', facecolor='b')\n",
    "ambiguousAxes.set_ylabel('Entries')\n",
    "ambiguousAxes.set_xlabel('Ring Present')\n",
    "ambiguousAxes.set_title('Initial Ambiguous')\n",
    "\n",
    "mplplot.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Do Some Simple Aggregation\n",
    "\n",
    "Finally, we can perform some simple aggregation of volunteer annotations, plotting the results over the images.\n",
    "\n",
    "We can group the data according to the `subjectId`, as each subject will have been classified by multiple volunteers, resulting in multiple classifications for each `subjectId`.\n",
    "\n",
    "In this example, for each subject we take the mean value for four parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subjectMeans = classificationsFrame.groupby(by=\"subjectId\")[[\"ringPresent\", \"ringRadius\", \"ringCentreX\", \"ringCentreY\"]].mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameter `ringPresent` corresponds to a binary \"yes/no\" question, and hence the resulting values are `1`s or `0`s. Taking the average value of these therefore indicates the fraction of volunteers responding with \"yes\".\n",
    "\n",
    "Hence, let's find the first 16 examples for which at least 80% of volunteers marked a ring, and plot the average ring drawn by volunteers over these images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "markedMuonSubjects = [Subject.find(sid) for sid in subjectMeans[subjectMeans.ringPresent > 0.8].index[:16]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, panels = mplplot.subplots(figsize=(16, 16), nrows=4, ncols=4)\n",
    "\n",
    "for subject, panel in zip(markedMuonSubjects, panels.flatten()):\n",
    "    panel.imshow(mplplot.imread(list(subject.locations[0].values())[0],format=\"jpeg\"))\n",
    "    ring = matplotlib.patches.Circle((subjectMeans.loc[int(subject.id)].ringCentreX, \n",
    "                                     subjectMeans.loc[int(subject.id)].ringCentreY), \n",
    "                                     subjectMeans.loc[int(subject.id)].ringRadius, \n",
    "                                     fc=\"none\", \n",
    "                                     ec=\"magenta\", \n",
    "                                     lw=3)\n",
    "    panel.set_title(subject.id)\n",
    "    panel.add_patch(ring)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "toc": {
   "nav_menu": {
    "height": "120px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": false,
   "threshold": 4,
   "toc_cell": true,
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
